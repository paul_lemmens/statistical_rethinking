p_grid <- seq( from=0 , to=1 , length.out=1000 )
prob_p <- rep( 1 , 1000 )
prob_data <- dbinom( 6 , size=9 , prob=p_grid )
posterior <- prob_data * prob_p
posterior <- posterior / sum(posterior)

samples <- sample(p_grid, prob = posterior, size = 1e4, replace = TRUE)
library(rethinking)
dens(samples)

PI(samples, prob = 0.8)
HPDI(samples, prob = 0.8)
PI(samples, prob = 0.95)
HPDI(samples, prob = 0.95)

chainmode(samples, adj = 0.01)

median(samples)
mean(samples)
