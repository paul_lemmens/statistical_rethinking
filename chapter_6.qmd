---
title: Solutions chapter 6 statistical rethinking
author: Paul Lemmens
format:
  html:
    toc: true
    toc-depth: 2
    theme: simplex
    fig-format: svg
    default-image-extension: svg
    dpi: 600
    fig-asp: 0.618
    fig-width: 16
    fig-height: 9
    embed-resources: false
mainfont: Source Sans Pro
monofont: Source Code Pro
fontsize: 14pt
filters:
   - lightbox
lightbox:
  match: auto
execute:
  echo: true
  message: false
  warning: false
---

```{r clear-workspace-and-prepare}
options(width=120)
require(tidyverse)
require(lemmens)
require(rethinking)
require(dagitty)
palette <- 'Dark2'
#palette <- 'Paired'
```

# Easy exercises

## 6e1-2

One way in which regression produces false inferences is when the data are not randomly sampled so that
there's (unknown) relationships between the intervention and outcome variable. Another way is via
multicollinearity which introduces spurious correlations between variables in the regression. The third way is
the post-treatment bias introduced when controlling for a score obtained after the treatment. The latter could
typically happen in my case when I'm using a follow-up score to tune the fitting of the effectiveness score.

## 6E3

The for elemental confounds are the fork (a confounder), the pipe, the inverted fork (the collider), and the
descendant. Their dependencies are that for the fork and the pipe a conditional dependency exists between X an
dY given the confounder Z. There is **independency** for the collider!

## 6e4

A biased sample is like conditioning on an unbalance that doesn't exist.

# Medium

## 6m1

```{r repro dag6.1}
#| fig-width: 3
#| fig-height: 3
#| fig-asp: 1
d6_1 <- dagitty('dag {
  U [unobserved]
  X -> Y
  X <- U <- A -> C -> Y
  U -> B -> C
}')
coordinates(d6_1) <- list(x = c(U = 0, X = 0, A = .5, B = .5, C = 1, Y = 1),
                          y = c(U = .25, X = 1, A = 0, B = .5, C = .25, Y = 1))
drawdag(d6_1, cex = 1.3, lwd = 3.5)
dag6m1 <- dagitty('dag {
  U [unobserved]
  V [unobserved]
  X -> Y
  X <- U <- A -> C -> Y
  U -> B -> C
  C <- V -> Y
}')
coordinates(dag6m1) <- list(x = c(U = 0, X = 0, A = .5, B = .5, C = 1, Y = 1, V = 1.25),
                          y = c(U = .25, X = 1, A = 0, B = .5, C = .25, Y = 1, V = .5))
drawdag(dag6m1, cex = 1.3, lwd = 3.5)
```

This now gives 2 additional path on top of the existing 3 paths, making a total of 5 paths. Given that the
original DAG had two variables `A` and `C` that needed closing and both additional paths still run via `C`, no
additional paths need to be closed.

```{r independencies and adjustment sets}
impliedConditionalIndependencies(dag6m1)
adjustmentSets(dag6m1, exposure = 'X', outcome = 'Y')
```

*This reasoning appears to be incorrect. Apparently, adding `V` removes `C` from variables to will close open
paths (or, at least, makes it not the most ideal variable to do so).* The thing that does change is that I now
need to condition on both `A` and `B` instead of on either `A` or `C` in the previous example.

## 6m2

```{r test}
#| eval = FALSE
N <- 200
bXY <- 0
bUY <- -1
bUZ <- -1
bZX <- 1

set.seed(4)
U <- rbern(N)
Z <- rnorm(N, bUZ * U)
X <- rnorm(N, bZX * Z)
Y <- rnorm(N, bXY*X + bUY * U)
m1 <- lm(Y ~ X)
summary(m1)
m2 <- lm(Y ~ X + Z)
summary(m2)
```

For this exercise, take the same approach as Richard took in the 2022 lectures around this topic by creating a
function that we can run repeatedly to draw as sample of estimated coefficients.

```{r simulate pipe}
f <- function(n = 100, bXZ = 10, bZY = 0.1) {
  X <- rnorm(n)
  u <- rnorm(n) # little bit of noise
  Z <- rnorm(n, bXZ * X + u)
  Y <- rnorm(n, bZY * Z + u)
  bx <- coef(lm(Y ~ X))['X']
  bxz <- coef(lm(Y ~ X + Z))['X']
  return(c(bx, bxz))
}

sim <- mcreplicate(1e4, f(), mc.cores = 8)

tibble(bx = sim[1, ], bxz = sim[2, ]) %>%
  pivot_longer(cols = everything(), values_to = 'mean', names_to = 'coef') %>%
  ggplot(mapping = aes(x = mean, color = coef)) +
    geom_density() +
    theme_lemmens + lemmens_brewer()
```

## 6m3

In the upper left DAG, there are three paths from `X` to `Y`: XY, XZY, and XZAY. In the latter path, Z changes
from the fork in the XZY path to a pipe. /I added this material after reviewing the chapter once more/. 

The XZY path can be and needs to be closed to get pure effect of X on Y. Z is the fork/confounder so
controlling this variable closes the backdoor path (because normally you should not control for the confounder
as that will remove the association between X and Y).

For the next path XZAY, A is a confounder and Z is a pipe (mediator). For the mediator the same story plays as
for the confounder: close the path by controlling for the variable. Given that Z now is a mediator and
confounder, controlling for this variable closes both backdoor paths that are there.

/Note, that the path needs to be closed because XZAY is another causal path with the arrow entering X/!

```{r upper left dag}
dag1 <- dagitty("dag{ X <- Z <- A -> Y <- X; Y <- Z }")
adjustmentSets(dag1, exposure = "X", outcome = "Y")
```

The upper right DAG changes the status of Z from fork to pipe in the XZY path and in the XZAY path, Z is a
collider and A again is a fork. Because a collider by default closes a backdoor path, we don't need to
anything here because A closes the backdoor path that (also) involves Z. Therefore, we don't need to condition
on any of the variables. This is even more so because the backdoor paths both do not have an arrow entering X
and therefore are no indirect causal paths that should be closed to get to the direct effect of X on Y.

```{r upper right dag}
dag2 <- dagitty("dag{ X -> Z <- A -> Y <- X; Y <- Z }")
adjustmentSets(dag2, exposure = "X", outcome = "Y")
```

The bottom left DAG again has three paths from `X` to `Y`: XY, XZY, and XAZY. In the XZY path, Z is a
collider, and in the XAZY path, A is once more a fork/confounder. Given that Z exists in both backdoor paths
and Z is identified as a collider that by default closes a path, no conditioning is needed.

```{r lower left dag}
dag3 <- dagitty("dag{ X <- A -> Z <- X -> Y; Y -> Z }")
## plot(dag3)
adjustmentSets(dag3, exposure = "X", outcome = "Y")
```

The final, bottom right DAG has three paths from X to Y: XY, XAZY, and XZY. The directions of the arrows make
A a fork and Z a pipe in XAZY. In XZY, Z is a pipe as well. Given the arrow entering X from A, an adjustment
(if possible) is needed to be able to get to the direct effect of X on Y. Therefore conditioning on A closes
the backdoor path(s).

```{r lower right dag}
dag4 <- dagitty("dag{ X <- A -> Z <- X -> Y; Y <- Z }")
adjustmentSets(dag4, exposure = "X", outcome = "Y")
```

>two causal paths from X to Y. The direct effect, and an indirect effect through Z. There is one additional path, X←A→Z→Y. Conditioning on A will close the path.


# Hard exercise

## 6h1

```{r explore wafflehouse}
data(WaffleDivorce)
wd <- WaffleDivorce %>% rename_with(tolower)
```

The interesting variables in the data set are: `loc` (location), `population`, `medianagemarriage`,
`marriage`, `divorce`, `wafflehouses`. This bunch can be joined together in a DAG, but for now focus on a
subset of it. Let's disregard the various states. Then the following DAG can be drawn up.

```{r waffle-dag}
#| fig-width: 3
#| fig-height: 3
#| fig-asp: 1
wd_dag <- dagitty('dag {
  W -> D <- M
  M <- P -> W
  A -> M
}')
coordinates(wd_dag) <- list(x = c(W = 0, D = 1, P = 0, M = 1, A = 0.5),
                            y = c(W = 1, D = 1, P = 0.5, M = 0.5, A = 0))
drawdag(wd_dag, cex = 1.3, lwd = 3.0)
```

This DAG shows one additional (backdoor) path `WPMD` in which `P` is a confounder (fork) and `M` is a mediator
(pipe). The second additional path feels a bit atypical because it doesn't revolve around the prime relation,
so we're ignoring that one becuase backdoor paths always have to involve both treatment and outcome
variable(s). 

Given that the question is to find the total causal effect (which I'm reading as the direct and indirect effects combined), we're not controlling for variables in the analysis. Should the direct effect remain, then the adjustment set would have been `M`; *`P` would be the other adjustment set*.

```{r waffle-adjust}
adjustmentSets(wd_dag, exposure = 'W', outcome = 'D')
```

The interesting side exercise would be to presume that age also influences population. This then creates a second indirect path `WPAMD` in which `P` is a mediator, `A` a confounder, and `M` a pipe. The adjustment set then becomes `A` (**This is incorrect as the code shows**!).

```{r alt-waffle-dag}
#| fig-width: 3
#| fig-height: 3
#| fig-asp: 1
alt_dag <- dagitty('dag {
  W -> D <- M
  M <- P -> W
  P <- A -> M
}')
coordinates(alt_dag) <- list(x = c(W = 0, D = 1, P = 0, M = 1, A = 0.5),
                            y = c(W = 1, D = 1, P = 0.5, M = 0.5, A = 0))
drawdag(alt_dag, cex = 1.3, lwd = 3.0)
adjustmentSets(alt_dag, exposure = 'W', outcome = 'D')
```

Now focus on the full model that is detailed in the last section of the chapter for which we determine the total causal influence. *In hindsight, I learn that here I should interpret total causal effect as the direct effect? So I should condition on S*.

```{r total-waffle}
#| fig-width: 3
#| fig-height: 3
#| fig-asp: 1
total_dag <- dagitty("dag {
    A -> D
    A -> M -> D
    A <- S -> M
    S -> W -> D
}")
coordinates(total_dag) <- list(x = c(W = 1, D = 1, M = 0.5, A = 0, S = 0),
                               y = c(W = 0, D = 1, M = 0.5, A = 1, S = 0))
drawdag(total_dag, cex = 1.3, lwd = 3.0)
adjustmentSets(total_dag, exposure="W" , outcome="D" )

## Re-use and expand code sample 5.1 and 5.10
d <- WaffleDivorce

# standardize variables
d$D <- standardize(d$Divorce)
d$M <- standardize(d$Marriage)
d$A <- standardize(d$MedianAgeMarriage)
d$W <- standardize(d$WaffleHouses)
d$S <- d$South

## m <- quap(
##     alist(
##         D ~ dnorm(mu, sigma),
##         mu <- a + bW*W + bS*S,
##         a ~ dnorm(0, 0.2),
##         bW ~ dnorm(0, 0.5),
##         bS ~ dnorm(0, 0.5),
##         sigma ~ dexp(1)
##     ) , data = d)
## precis(m)
## plot(m)
```

So by closing the backdoor path via `S` it is clear that the effect of number of waffle houses on divorce rate
is really small. That southerness of a state has a much stronger effect, however, cannot be determined because
that would be a table 2 fallacy.

## 6h3

This assignment is similar to the first hard exercise, so follow this pattern before moving towards the more complicated ones. In the DAG below, A represent the area; F food, W weight, and S groupsize.

```{r fox-dag}
#| fig-width: 3
#| fig-height: 3
#| fig-asp: 1
fox_dag <- dagitty("dag {
    A -> F -> W
    F -> S -> W
}")
coordinates(fox_dag) <- list(x = c(A = 0.5, F = 0, S = 1, W = 0.5),
                             y = c(A = 0, F = 0.5, S = 0.5, W = 1))
drawdag(fox_dag, cex = 1.3, lwd = 3.0)
```

In this DAG, food is a mediator in the direct effect (frontdoor path). In the (sole) alternative, backdoor path AFSW, food and group size both are mediators. Given that there is no direct path between area and weight, we cannot condition on food because that would close the direct path. Moreover, conditioning on group size to close the indirect path is also not needed because there is no arrow entering A (as intervention) so there are no non-causal paths that need to be controlled for.

```{r fox dag adjustments}
adjustmentSets(fox_dag, exposure = 'A', outcome = 'W')
```

```{r fox data}
data(foxes)
d <- foxes %>%
  mutate(across(-group, standardize)) %>%
  rename(F = avgfood,
         S = groupsize,
         A = area,
         W = weight)
```

Plot the densities for these variables to determine appropriate priors.

```{r fox-densities}
foxes %>%
  pivot_longer(cols = -group, names_to = 'variable', values_to = 'value') %>%
  ggplot(mapping = aes(x = value, fill = variable)) +
    geom_density() +
    facet_wrap(~ variable, scales = 'free') +
    theme_lemmens + lemmens_brewer()
```


```{r fox model}
m <- quap(
    alist(
        W ~ dnorm(mu, sigma),
        mu <- a + bA*A + bF*F + bS*S,
        a ~ dnorm(0, 0.2),
        bA ~ dnorm(0, 0.2),
        bF ~ dnorm(0, 0.3),
        bS ~ dnorm(0, 0.2),
        sigma ~ dexp(1)
    ) , data = d)
```

First, extract the prior from the model to see if they make any sense, given the potential values for the parameters that cover the space. Re-use R-code 5.34 for this.

```{r fox-priors}
prior <- extract.prior(m)
xseq <- c(-2, 2)
mu <- link(m, post = prior, data = list(A = xseq, F = xseq, S = xseq))
plot(NULL, xlim = xseq, ylim = xseq)
for (i in 1:50) lines(xseq, mu[i, ], col = col.alpha('black', 0.2))
```

So while I don't (yet 🤷🏻) know how to read the prior plot when it is based on more than one predictive variable, the plot with priors that are at `dnorm(0, 0.2)` for `A` and `S` (standardized) variables seems to give a decent prior predictive simulation; for `F`, I'm taking 0.3.

We can then assess the posterior distribution to learn about the total causal effect of area on fox weight.

```{r area-fox-weight}
precis(m)
plot(m)
```

The implication is that larger areas seem to have a positive effect on foxes' body weight but not very much larger than average food (which makes sense) and zero is part of the posterior meaning that the effect could also be negligible.

To tease apart these two variables, we can use a counterfactual wherein we set avgfood, for instance, at 0 (the average expressed in standard deviations), and then manipulate the area to also be larger than in the data present.

```{r fox counterfactual}
sim_dat <- data.frame(A = seq(from = -2, to = 3, length.out = 40),
                      F = 0, S = 0)
s <- sim(m, data = sim_dat, vars = c('F', 'S', 'W'))
```

```{r fox-counterfact-plot}
plot(sim_dat$A, colMeans(s$W), ylim = c(-2, 2), type = 'l', xlab = '\nManipulated A',
     ylab = 'Counterfactual W\n')
shade(apply(s$W, 2, PI), sim_dat$A)
mtext('Effect of area on weight at constant food and group size\n')
```

This figure confirms that even for very large areas, the counterfactual weight doesn't increase a lot above the average. 

Now for good measure replicate the approach from R code 5.19 to confirm whether the staged approach does or does not change things here.

```{r staged-fox-counterfactual}
m <- quap(
    alist(
      ## W <- F <- A, A -> F -> S -> W
        W ~ dnorm(mu, sigma),
        mu <- a + bA*A + bF*F + bS*S,
        a ~ dnorm(0, 0.2),
        bA ~ dnorm(0, 0.2),
        bF ~ dnorm(0, 0.3),
        bS ~ dnorm(0, 0.2),
        sigma ~ dexp(1),
      ## A -> F -> S
        S ~ dnorm(mu_S, sigma_S),
        mu_S <- aS + bFS * F + bAF*A,
        aS ~ dnorm(0, 0.2),
        bAF ~ dnorm(0, 0.2),
        bFS ~ dnorm(0, 0.2),
        sigma_S ~ dexp(1)
    ), data = d)
sim_dat <- data.frame(A = seq(from = -2, to = 3, length.out = 40),
                      F = 0, S = 0)
s <- sim(m, data = sim_dat, vars = c('F', 'S', 'W'))
plot(sim_dat$A, colMeans(s$W), ylim = c(-2, 2), type = 'l', xlab = '\nManipulated A',
     ylab = 'Counterfactual W\n')
shade(apply(s$W, 2, PI), sim_dat$A)
mtext('Effect of area on weight at constant food and group size\n')
```

